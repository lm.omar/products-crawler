<?php

namespace App\Tests;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LoginTest extends WebTestCase
{
    public function testLoginPageIfIsDisplayed(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Login');
    }

    public function testAdminPage(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET','/admin');

        $userRepo = static::getContainer()->get(UserRepository::class);
        $user = $userRepo->findOneByEmail('om@gmail.com');
        $client->loginUser($user);
        $this->assertResponseIsSuccessful();
        $this->assertSelectorExists("//table[@id='productsList']");
    }

}
