<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Validation;

#[AsCommand(
    name: 'app:create-user',
    description: 'Add a short description for your command',
)]
class CreateUserCommand extends Command
{
    public function __construct(private UserPasswordHasherInterface $hasher, private ManagerRegistry $manager)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('email', InputArgument::OPTIONAL, "What the email address of user ? ( user@domain.com )")
            ->addArgument('password', InputArgument::OPTIONAL, 'Password')
            ->addArgument('isAdmin', InputArgument::OPTIONAL, 'Is the user an Admin? ');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $user = new User();
        foreach ($this->getDefinition()->getArguments() as $argument) {
            switch ($argument->getName()) {
                case 'email':
                    $emailValue = $io->ask(
                        $argument->getDescription(),
                        $argument->getDefault(),
                        Validation::createCallable(new Email())
                    );
                    $user->setEmail($emailValue);
                    break;
                case 'password':
                    $password = $io->ask(
                        $argument->getDescription(),
                        $argument->getDefault(),
                        Validation::createCallable(new Length(null, 6))
                    );
                    $hashPassword = $this->hasher->hashPassword($user, $password);
                    $user->setPassword($hashPassword);
                    break;
                case 'isAdmin':
                    $isAdmin = $io->ask(
                        $argument->getDescription(),
                        $argument->getDefault(),
                        Validation::createCallable(new Choice([ 'choices' => array('yes', 'no')]))
                    );
                    if ($isAdmin == 'yes') {
                        $user->setRoles(array('ROLE_ADMIN'));
                    }
                    break;
            }
        }

        $this->manager->getManager()->persist($user);
        $this->manager->getManager()->flush();

        $io->success(sprintf("User with email: %s created successfully .", $user->getEmail()));

        return Command::SUCCESS;
    }
}
