<?php

namespace App\Service;

use App\Entity\Brand;
use App\Entity\Product;
use Doctrine\Persistence\ManagerRegistry;
use DOMElement;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CrawlerService
{
    private static string $url = 'https://www.disway.com';
    private static string $path = '/r%C3%A9seau/switch/';
    private ManagerRegistry $manager;
    private HttpClientInterface $httpClient;

    public function __construct(ManagerRegistry $registry, HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
        $this->manager = $registry;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function run(): void
    {
        try {
            //$html = file_get_contents(self::$url . self::$path);
            $response = $this->httpClient->request(
                'GET',
                self::$url . self::$path
            );
            $crawler = new Crawler($response->getContent());
            /* iterate over all pages */
            $nextPage = $crawler->filterXPath("//div[@class='PLP_panel-footer']//a[@class='Button_base Button_small Paging_arrow Paging_right']");
            $nextPageRes = current($nextPage->extract(['aria-disabled']));
            $this->iterateProducts(self::$url . self::$path);
            while ($nextPageRes == "") {
                $page = $nextPage->extract(["href"]);
                $this->iterateProducts(self::$url . current($page));
            }
        } catch (\Exception $exception) {
            print_r($exception->getMessage());
            echo "\n";
            print_r($exception->getTraceAsString());
            die('end');
        }
    }


    private function iterateProducts(string $page): void
    {
        try {
            $response = $this->httpClient->request(
                'GET',
                $page
            );
            $crawler = new Crawler($response->getContent());
            /* iterate over products in the current page */
            $products = $crawler->filterXPath("//div[@class='PLP_list']/div[@class='PLP_item']");
            $productUrl = "";
            /** @var DOMElement $item */
            foreach ($products as $item) {
                $cr = new Crawler($item);
                $productUrl = self::$url . $cr->filterXPath("//div[@class='PLP_product-description']/a")->attr('href');
                $response = $this->httpClient->request(
                    'GET',
                    $productUrl
                );
                $htmlProduct = $response->getContent();
                $productCrawler = new Crawler($htmlProduct);
                $productName = $productCrawler->filterXPath("//div[contains(@class,'Containers_content-box')]//h1")->text();
                $productRef = $productCrawler->filterXPath("//div[contains(@class,'Details_item-number-body')]/span[last()]")->text();
                $productBrandName = $productCrawler->filterXPath("//div[@class='Details_block-specs']/dl/dt[contains(.,'Marque')]/following-sibling::dd")->text();
                $productImage = $productCrawler->filterXPath("//div[contains(@class,'MediaGallery_thumbnails-as-images')]/div[@class='Gallery_feature']//img")->extract(['src']);
                $productDescription = "";
                if ($productCrawler->filterXPath("//div[@class='Details_description']")->count() > 0) {
                    $productDescription = $productCrawler->filterXPath("//div[@class='Details_description']")->html();
                }
                $productSpecs = "";
                if ($productCrawler->filterXPath("//div[@class='Details_block-specs']/dl/dt[contains(.,'Marque')]")->count() > 0) {
                    $productSpecs = $productCrawler->filterXPath("//div[@class='Details_block-specs']/dl/dt[contains(.,'Marque')]/..")->html();
                }
                $brand = $this->checkBrand($productBrandName);
                $product = new Product();
                $product->setName($productName)
                    ->setBrand($brand)
                    ->setPictureUrl(current($productImage))
                    ->setRef($productRef)
                    ->setDescription($productDescription)
                    ->setUrl($productUrl)
                    ->setSpecs($productSpecs);
                $this->manager->getManager()->persist($product);
                $this->manager->getManager()->flush();
                echo $productName . "\n";
            }
        } catch (\Exception $exception) {
            print $productUrl . "\n";
            print_r($exception->getMessage());
            echo "\n";
            print_r($exception->getTraceAsString());
            die('end');
        }

    }

    /**
     * @param string $brandName
     * @return Brand
     */
    private function checkBrand(string $brandName): Brand
    {
        $brand = $this->manager->getRepository(Brand::class)->findOneBy(['name' => $brandName]);
        if (!$brand instanceof Brand) {
            $brand = new Brand();
            $brand->setName($brandName);
            $this->manager->getManager()->persist($brand);
            $this->manager->getManager()->flush();
        }
        return $brand;

    }


}
