<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Service\CrawlerService;
use DOMElement;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
#[Route('/admin')]
class HomeController extends AbstractController
{
    #[Route('/{page<\d+>}', name: 'app_home')]
    public function index(ProductRepository $repository, int $page = 1): Response
    {
        $queryBuilder = $repository->getProducts();

        $paginator = new Pagerfanta(new QueryAdapter($queryBuilder));
        $paginator->setMaxPerPage(25);
        $paginator->setCurrentPage($page);
        return $this->render('home/index.html.twig', [
            'pager' => $paginator
        ]);
    }

    #[Route('/view/{id}', name: 'product_view')]
    public function view(Product $product): Response
    {
        return $this->render('home/view.html.twig', ['product' => $product]);
    }
}
